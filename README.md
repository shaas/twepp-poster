# TWEPP Poster
The three-channel board poster which will be presented at TWEPP 2019.
The poster is [based on a LaTeX class](https://www.overleaf.com/articles/decaf-poster/ryfhdpmwcnpd) which was taken from the [Overleaf](https://www.overleaf.com/) project site. 

